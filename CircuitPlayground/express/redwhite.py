# SPDX-FileCopyrightText: 2021 ladyada for Adafruit Industries
# SPDX-License-Identifier: MIT

import time
"""This example lights up all the NeoPixel LEDs red."""
from adafruit_circuitplayground import cp
max=60
while True:
    for x in range(max):
        cp.pixels.fill((x, 0, 0))
    for x in range(max):
        cp.pixels.fill((max-x, 0, 0))
    for x in range(max):
        cp.pixels.fill((x, x, x))
    for x in range(max):
        cp.pixels.fill((max-x, max-x, max-x))

#    cp.pixels.fill((255, 0, 0))
#    time.sleep(1)
#    cp.pixels.fill((60, 60, 60))
#    time.sleep(1)
