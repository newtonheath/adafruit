from adafruit_circuitplayground import cp
import time
max=60
while True:
    for x in range(max):
        cp.pixels.fill((x, 0, 0))
        time.sleep(0.05)
    for x in range(max):
        cp.pixels.fill((max-x, 0, 0))
        time.sleep(0.05)
    for x in range(max):
        cp.pixels.fill((x, x, x))
        time.sleep(0.05)
    for x in range(max):
        cp.pixels.fill((max-x, max-x, max-x))
        time.sleep(0.05)
